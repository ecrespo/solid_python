from abc import ABC, abstractmethod


class Car(ABC):
    def __init__(self, brand):
        self.brand = brand

    @abstractmethod
    def print_engine_type(self):
        pass


class ElectricCar(Car):
    def print_engine_type(self):
        print('Electric')


class GasolineCar(Car):
    def print_engine_type(self):
        print('Gasoline')


class HybridCar(Car):
    def print_engine_type(self):
        print('Hybrid')


tesla_car = ElectricCar('Tesla Model 3')
audi_car = GasolineCar('Audi A3')
myvi_car = GasolineCar('Myvi')
jazz_car = HybridCar('Honda Jazz Hybrid')

for car in [tesla_car, audi_car, myvi_car, jazz_car]:
    car.print_engine_type()  # print Electric, Gasoline, Gasoline, Hybrid

# The Open-Closed Principle Explained in Python

Software components (classes and functions) should be open for extension but closed for modification.

Software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification

El Principio de Abierto/Cerrado indica que las clases deberían estar abiertas para su extensión, pero cerradas para su modificación. En otros términos, el código debería estar escrito de tal manera que, a la hora de añadir nuevas funcionalidades, no se deba modificar el código escrito previamente, que pueda estar siendo utilizado por otros usuarios



* El software debería estar abierto a extensión y cerrado a modificación

* Cómo:
    * No dependiendo de implementaciones especificas
* Finalidad:
    * Fácil de añadir nuevos caso de uso

Beneficios de interfaces:

* No modifica árbol jerarquía.
* Permite implementar N.

Beneficios abstract class:

* Permite patrón template method empujando lógica al modelo.
* Problema: Dificultad de trazar.
* Getter privados (tell don’t ask).



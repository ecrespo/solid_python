class Car:
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def buy(self, cash):
        if cash == self.price:
            print("Buying {}".format(self.name))
        else:
            print("Sorry, you don't have enough money")


class Discount():
    def __init__(self, discount):
        self.discount = discount

    def buy_with_discount(self, car):
        print("Buying {} with {}% discount".format(car.name, self.discount))


class Discount20(Discount):
    def __init__(self):
        super().__init__(20)

    def buy_with_discount(self, cash, car):
        if int(0.8*car.price) == int(cash):
            return super().buy_with_discount(car)


class Discount30(Discount):
    def __init__(self):
        super().__init__(30)

    def buy_with_discount(self, cash, car):
        if int(0.7*car.price) == int(cash):
            return super().buy_with_discount(car)


if __name__ == '__main__':
    car = Car('BMW', 100000)
    Discount20().buy_with_discount(80000, car)
    Discount30().buy_with_discount(70000, car)

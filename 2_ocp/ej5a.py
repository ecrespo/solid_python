class Car:
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def buy(self, cash):
        if cash == self.price:
            print("Buying {}".format(self.name))
        else:
            print("Sorry, you don't have enough money")

    def buy_with_discount(self, cash):
        if int(0.8*self.price) == int(cash):
            print("Buying {} with 20%% discount".format(self.name))


if __name__ == '__main__':
    car = Car('BMW', 100000)
    car.buy_with_discount(80000)

class Car:
    def __init__(self, brand):
        self.brand = brand

    def print_engine_type(self):
        if self.brand == 'Tesla Model 3':
            print('Electric')
        elif self.brand == 'Audi A3':
            print('Gasoline')
        elif self.brand == 'Myvi':
            print('Gasoline')
        elif self.brand == 'Some hybrid brand':
            # NEED TO ADD THIS EVERYTIME A NEW BRAND SURFACED
            print('Hybrid')


tesla_car = Car('Tesla Model 3')
audi_car = Car('Audi A3')
myvi_car = Car('Myvi')

for car in [tesla_car, audi_car, myvi_car]:
    car.print_engine_type()  # print Electric, Gasoline, Gasoline

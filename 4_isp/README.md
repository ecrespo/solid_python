# The Interface Segregation Principle (ISP) Explained in Python

Many client specific interfaces are better than one general purpose interface
The principle of interface separation advises that interfaces should be small for cohesion.
Make small interfaces specific to subclasses. Subclasses should not be forced to implement interfaces they do not use.


No client should be forced to depend on methods it does not use
My Understanding: As the definition suggests, if the function is not needed by the subclass, we probably shouldn’t define that interface to be inherited in the parent class.


* Ningún cliente debería verse forzado a depender de métodos que no usa.
* Como: Definir contratos de interfaces basándonos en los clientes que las usan y no en las implementaciones que pudiéramos tener.
* Evitar Header Interface promoviendo Role Interface.
* LAS INTERFACES PERTENECEN A LOS CLIENTES
* Finalidad:
    * Alta cohesión y bajo acoplamiento estructural.
  
El Principio de Segregación de Interfaz establece que los clientes no deberían ser forzados a depender de métodos que no utilizan y, por tanto, sugiere la creación de interfaces o clases específicas para dichos clientes. En el apartado anterior, se ha añadido una nueva clase Crow que describe un cuervo. Pero en esa definición hay un problema: el cuervo no sabe nadar y la clase abstracta Bird nos obliga a definir swim().

"Los clientes no deberían estar obligados a depender de interfaces que no utilicen."
El principio de segregación de la interfaz nos indica que ninguna clase debería depender de métodos que no usa. Cuando creemos interfaces (clases en lenguajes interpretados como Python) que definan comportamientos, es importante estar seguros de que todas los objetos que implementen esas interfaces/clases se vayan a necesitar, de lo contrario, es mejor tener varias interfaces/clases pequeñas.

Una forma de no violar este principio en Python es aplicando duck typing. Este concepto viene decir que los métodos y propiedades de un objeto determinan su validez semántica, en vez de su jerarquía de clases o la implementación de una interfaz específica.


Cree interfaces de grano fino que sean específicas del cliente. Los clientes no deberían verse obligados a depender de interfaces que no utilicen. Este principio se ocupa de las desventajas de implementar grandes interfaces.



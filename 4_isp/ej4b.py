class Flyable:
    def fly(self):
        pass


class Runnable:
    def run(self):
        pass


class Swimable:
    def swim(self):
        pass


class Dog(Runnable, Swimable):
    def run(self):
        print('Dog can run')

    def swim(self):
        print('Dog can swim')

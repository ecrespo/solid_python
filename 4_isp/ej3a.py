# https://betterprogramming.pub/the-interface-segregation-principle-isp-explained-in-python-46e173241642
from abc import ABC, abstractmethod


class Car(ABC):
    @abstractmethod
    def __init__(self, name):
        """Please implement intialization of a car"""

    @abstractmethod
    def accelerate(self):
        """Please implement accelerating of a car"""

    @abstractmethod
    def turboAccelerate(self, turbo):
        """Please implement turboaccelerating of a car"""


class RegularCar(Car):
    def __init__(self, name):
        self.name = name
        self.speed = 0

    def accelerate(self):
        self.speed += 1
        print("Car %s is accelerating" % self.name)

    def turboAccelerate(self, turbo):
        raise Exception("Regular car has no turbo")


class SportsCar(Car):
    def __init__(self, name):
        self.name = name
        self.speed = 0

    def accelerate(self):
        self.speed += 1
        print("Car %s is accelerating" % self.name)

    def turboAccelerate(self, turbo):
        self.speed += turbo
        print("Car %s is accelerating with turbo %d" % (self.name, turbo))


if __name__ == '__main__':

    car = RegularCar('BMW')
    car.accelerate()

    autoCar = SportsCar('Audi')
    autoCar.turboAccelerate(2)

    # This line would raise an exception.
    # This is a violation of ISP since the class is implementing a function that is not used
    car.turboAccelerate(2)

class Animal:
    def fly(self):
        raise NotImplementedError

    def run(self):
        raise NotImplementedError

    def swim(self):
        raise NotImplementedError

    class Dog(Animal):
        def fly(self):
            raise Exception('The dog cannot fly')

        def run(self):
            print('Dog can run')

        def swim(self):
            print('Dog can swim')

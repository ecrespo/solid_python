# The Liskov Substitution Principle (LSP) Explained in Python


Las funciones que utilicen punteros o referencias a clases base deben ser capaces de usar objetos de clases derivadas sin saberlo.


if S is a subtype of T, then objects of type T may be replaced with objects of type S (i.e., an object of type T may be substituted with any object of a subtype S) without altering any of the desirable properties of the program (correctness, task performed, etc.)

El principio de Sustitución de Liskov, obtiene su nombre de Barbara Liskov. Este principio está relacionado con el anterior en lo que a la extensibilidad de las clases se refiere, y viene a decir que dada una instancia de una clase B, siendo esta un subtipo de una clase A, debemos poder sustituirla por una instancia de la clase A sin mayor problema.

En los lenguajes orientados a objetos de tipado estático, este principio describe principalmente una regla sobre una relación entre una subclase y una superclase. Cuando hablamos de lenguajes de tipado dinámico como Python, nos interesa qué mensajes responde ese objeto en lugar de a qué clase pertenece.


La idea principal detrás del principio de subtitulación de Liskov es que, para cualquier clase, un cliente debería poder usar cualquiera de sus subtipos de manera indistinguible, sin siquiera darse cuenta y, por lo tanto, sin comprometer el comportamiento esperado en tiempo de ejecución. Esto significa que los clientes están completamente aislados y desconocen los cambios en la jerarquía de clases.


El Principio de Substitución de Liskov establece que las clases deberían ser sustituibles por instancias de sus subclases. Para ilustrar este principio, se considera la posibilidad de que se puedan añadir nuevas aves en un futuro. Para ello, una buena práctica consiste en añadir clase abstracta Bird y que las aves como Duck implementen sus métodos. A partir de aquí también se puede definir, a partir de Bird, una subclase de cuervo Crow.




Si S es un subtipo de T, instancia de T deberían poderse sustituir por instancias de S sin alterar las propiedades del programa.
Cómo:
* El comportamiento de subclases debe respetar el contrato de las superclases.
Finalidad:
* Mantener correctitude para poder aplicar OCP.

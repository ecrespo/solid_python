class Car:
    def __init__(self, name):
        self.name = name
        self.gears = ["N", "1", "2", "3", "4", "5", "6", "R"]
        self.speed = 0
        self.gear = "N"

    def changeGear(self, gear):
        if (gear in self.gears):
            self.gear = gear
            print("Car %s is in gear %s" % (self.name, self.gear))

    def accelerate(self):
        if (self.gear == "N"):
            print("Error: Car %s is in gear N" % self.name)
        else:
            self.speed += 1
            print("Car %s is accelerating" % self.name)


class SportsCar(Car):
    def __init__(self, name):
        super().__init__(name)
        self.turbos = [2, 3]

    def accelerate(self, turbo):
        if (self.gear == "N"):
            print("Error: Car %s is in gear N" % self.name)
        else:
            if (turbo in self.turbos):
                self.speed += turbo
                print("Car %s is accelerating with turbo %d" %
                      (self.name, turbo))


if __name__ == '__main__':

    # This works
    car = Car('BMW')
    car.changeGear("1")
    car.accelerate()

    # But we can't replace Car with SportsCar without breaking changes
    # If we do this, the program crashes:
    autoCar = SportsCar('Audi')
    autoCar.changeGear("1")
    autoCar.accelerate()

class Rectangle(object):

    def getWidth(self):
        return self._width

    def setWidth(self, width):
        self._width = width

    def getHeight(self):
        return self._height

    def setHeight(self, height):
        self._height = height

    def calculateArea(self):
        return self._width * self._height


class Square(Rectangle):

    def setWidth(self, width):
        self._width = width
        self._height = width

    def setWidth(self, height):
        self._height = height
        self._width = height


class TestRectangle(unittest.TestCase):

    def setUp(self):
        pass

    def test_calculateArea(self):
        r = Rectangle()
        r.setWidth(5)
        r.setHeight(4)
        self.assertEqual(r.calculateArea(), 20)

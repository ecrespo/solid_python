class Car:
    def charge(self):
        pass

    def pump_petrol(self):
        pass


class Tesla(Car):
    def charge(self):
        print('Charging...')

    def pump_petrol(self):
        raise Exception('Not a gasoline car')


class AudiA3(Car):
    def charge(self):
        raise Exception('Not an electric car')

    def pump_petrol(self):
        print('Pumping...')


def recharge_car(car):
    car.charge()


tesla_car = Tesla()
audi_a3_car = AudiA3()
recharge_car(tesla_car)  # print Charging...
recharge_car(audi_a3_car)  # THIS RAISES EXCEPTION!

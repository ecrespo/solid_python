class Car:
    pass


class ElectricCar(Car):
    def charge(self):
        pass


class GasolineCar(Car):
    def pump_petrol(self):
        pass


class Tesla(ElectricCar):
    def charge(self):
        print('Charging...')


class AudiA3(GasolineCar):
    def pump_petrol(self):
        print('Pumping...')


def recharge_car(electric_car):
    electric_car.charge()


def pump_petrol(gasoline_car):
    gasoline_car.pump_petrol()


tesla_car = Tesla()
audi_a3_car = AudiA3()
recharge_car(tesla_car)  # print Charging...
pump_petrol(audi_a3_car)  # print('Pumping...')

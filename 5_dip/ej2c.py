from abc import ABC, abstractmethod


class Eatable(ABC):
    @abstractmethod
    def eat(self):
        return NotImplemented


class Apple(Eatable):
    def eat(self):
        print(f"Eating Apple. Transferring {5} units of energy to brain...")


class Chocolate(Eatable):
    def eat(self):
        print(
            f"Eating Chocolate. Transferring {10} units of energy to brain...")


class Robot:
    def get_energy(self, eatable: Eatable):
        eatable.eat()


if __name__ == '__main__':
    robot = Robot()
    robot.get_energy(Apple())

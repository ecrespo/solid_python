from abc import ABC, abstractmethod


class Garage(ABC):
    def __init__(self):
        self.cars = []

    @abstractmethod
    def add_car_to_garage(self):
        pass

    @abstractmethod
    def get_cars_that_needs_charge(self):
        pass


class MyGarage(Garage):
    def add_car_to_garage(self, name, capacity):
        self.cars.append((name, capacity))

    def get_cars_that_needs_charge(self):
        return [car for car in self.cars if car[1] < 50]


class ChargingStation:
    def __init__(self, garage):
        num = len(garage.get_cars_that_needs_charge())
        print(f'We need to charge {num} car(s)')


gar = MyGarage()
gar.add_car_to_garage('Tesla1', 50)
gar.add_car_to_garage('Tesla2', 30)
gar.add_car_to_garage('Tesla3', 80)
station = ChargingStation(gar)  # print We need to charge 1 car(s)

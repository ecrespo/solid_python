class Apple:
    def eat(self):
        print(f"Eating Apple. Transferring {5} units of energy to brain...")


class Chocolate:
    def eat(self):
        print(
            f"Eating Chocolate. Transferring {10} units of energy to brain...")


class Robot:
    def get_energy(self, eatable: str):
        if eatable == "Apple":
            apple = Apple()
            apple.eat()
        elif eatable == "Chocolate":
            chocolate = Chocolate()
            chocolate.eat()


if __name__ == '__main__':
    robot = Robot()
    robot.get_energy("Apple")

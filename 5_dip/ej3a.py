# low level
class LightBulb:
    def turn_on(self):
        print("light bulb turned on")

    def turn_of(self):
        print("light bulb turn off")

# high level


class ElectricPowerSwitch:

    def __init__(self, light_bulb):
        self.light_bulb = light_bulb
        self.on = False

    def press(self):
        if self.on:
            self.light_bulb.turn_off()
            self.on = False
        else:
            self.light_bulb.turn_on()
            self.on = True

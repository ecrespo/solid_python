class AnotherGarage(Garage):
    def add_car_to_garage(self, name, capacity):
        self.cars.append({'name': name, 'capacity': capacity})

    def get_cars_that_needs_charge(self):
        return [car for car in self.cars if car['capacity'] < 60]


gar = AnotherGarage()
gar.add_car_to_garage('Tesla1', 50)
gar.add_car_to_garage('Tesla2', 30)
gar.add_car_to_garage('Tesla3', 80)
station = ChargingStation(gar)  # print We need to charge 2 car(s)
# we don't need to modify implementation of ChargingStation

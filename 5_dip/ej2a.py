class Apple:
    def eat(self):
        print(f"Eating Apple. Transferring {5} units of energy to brain...")


class Robot:
    def get_energy(self):
        apple = Apple()
        apple.eat()


if __name__ == '__main__':
    robot = Robot()
    robot.get_energy()

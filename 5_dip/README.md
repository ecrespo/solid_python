DIP (Dependency Inversion Principle)
Módulos de alto nivel no deberían depender de los de bajo nivel. Ambos deberían depender de abstracciones.

Como:

Inyectar dependencias (parámetros recibidos idealmente en constructor).
Depender de las interfaces (contratos) de estas dependencias y no de las implementaciones concretas.
LSP como premisa.
Finalidad:

Facilitar la modificación y substitución de implementaciones.
Mejor testabilidad de clases.


La dependencia debe estar en abstracciones, no en concreciones. Los módulos de alto nivel no deben depender de módulos de bajo nivel. Tanto las clases de nivel bajo como las de alto nivel deberían depender de las mismas abstracciones. Las abstracciones no deberían depender de los detalles. Los detalles deben depender de abstracciones.


"Los módulos de alto nivel no deben depender de módulos de bajo nivel. Ambos deben depender de abstracciones. "
"Las abstracciones no deben depender de concreciones. Los detalles deben depender de abstracciones."
Quinto y último de los principios, la inversión de dependencia, cuyo objetivo principal es desacoplar nuestro código de sus dependencias directas.  Este principio viene a decir que las clases de las capas superiores no deberían depender de las clases de las capas inferiores, sino que ambas deberían depender de abstracciones. A su vez, dichas abstracciones no deberían depender de los detalles, sino que son los detalles los que deberían depender de las mismas.



El último principio llamado Principio de Inversión de Dependencias se puede separar en dos enunciados. Por un lado, indica que las abstracciones no deberían depender de los detalles, pues los detalles deberían depender de las abstracciones.  Por otro lado, indica que las clases de alto nivel no deberían depender de clases de bajo nivel, dado que ambas deberían depender de abstracciones. En resumen, hay que depender de las abstracciones.


High-level modules should not import anything from low-level modules. Both should depend on abstractions (e.g., interfaces).
Abstractions should not depend on details. Details (concrete implementations) should depend on abstractions.
My Understanding: Similar to API calls, changing the low-level implementation of an API shouldn’t impact the caller. A class shouldn’t need to understand the inner workings of another class for it to function properly.


Depend upon Abstractions. Do not depend upon concretions.
High-level modules should not depend on the low-level modules.
Both should depend on abstractions.

https://github.com/ArjanCodes/betterpython/tree/main/9%20-%20solid

https://haseebkamal.com/the-dependency-inversion-principle-explained-in-python/


https://medium.com/@didemyaniktepe/solid-principles-with-python-97b5e7250ed7#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6ImNlYzEzZGViZjRiOTY0Nzk2ODM3MzYyMDUwODI0NjZjMTQ3OTdiZDAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJuYmYiOjE2NDg5NDQwMDcsImF1ZCI6IjIxNjI5NjAzNTgzNC1rMWs2cWUwNjBzMnRwMmEyamFtNGxqZGNtczAwc3R0Zy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMzA0ODgwMTUxNTE4Mzk4NTA5MyIsImVtYWlsIjoiZWNyZXNwb0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMjE2Mjk2MDM1ODM0LWsxazZxZTA2MHMydHAyYTJqYW00bGpkY21zMDBzdHRnLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwibmFtZSI6IkVybmVzdG8gQ3Jlc3BvIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdpNzZJNGhTTXQ3U1hYbzZjdHdaNDloeDlKbTF3cU93UXZiTU0zbWtHMD1zOTYtYyIsImdpdmVuX25hbWUiOiJFcm5lc3RvIiwiZmFtaWx5X25hbWUiOiJDcmVzcG8iLCJpYXQiOjE2NDg5NDQzMDcsImV4cCI6MTY0ODk0NzkwNywianRpIjoiY2Q2Y2Y4MjE5NGM1MzhmMmI3ZGI5YTgzN2VhMTYwZjkwNTVlOGNiZSJ9.1Sly9SyVfwisZVXcorD6JjXyyXE8qktKUiAMKUnCFOmpuaGU21kTtbpD7cZ6XwBtqyRu39XI0EoqjwwUStQYBaOUmNwi4o9igy1PMDB2BuR4nIlj8m5uaRRkKx4HEU1tkDl7syoWSFDsWkcxsMLyPtXCS3RBqUY-rZZxkttz7alELE5IHVat8YA9CatbIq_CKUJ3SMxi_uFN6OIQwha7WjMRPQ33Bc6D93Rrdb7Q_0jvWQTw1fJHI5jjH38bHzJEdPlkymBkWz41_7PFr8wnNaXi1jrW7ToUTSqF9bUiU4JElVwUlKbgEaFMp3HQt01rotg6IekX3rzZ_duKRR8E7g


https://medium.com/geekculture/the-s-o-l-i-d-principles-in-python-a041c5aa9969#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6ImQ2M2RiZTczYWFkODhjODU0ZGUwZDhkNmMwMTRjMzZkYzI1YzQyOTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJuYmYiOjE2NDY5NjE2MDYsImF1ZCI6IjIxNjI5NjAzNTgzNC1rMWs2cWUwNjBzMnRwMmEyamFtNGxqZGNtczAwc3R0Zy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMzA0ODgwMTUxNTE4Mzk4NTA5MyIsImVtYWlsIjoiZWNyZXNwb0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMjE2Mjk2MDM1ODM0LWsxazZxZTA2MHMydHAyYTJqYW00bGpkY21zMDBzdHRnLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwibmFtZSI6IkVybmVzdG8gQ3Jlc3BvIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdpNzZJNGhTTXQ3U1hYbzZjdHdaNDloeDlKbTF3cU93UXZiTU0zbWtHMD1zOTYtYyIsImdpdmVuX25hbWUiOiJFcm5lc3RvIiwiZmFtaWx5X25hbWUiOiJDcmVzcG8iLCJpYXQiOjE2NDY5NjE5MDYsImV4cCI6MTY0Njk2NTUwNiwianRpIjoiNWM2ZWFkNjRjNjcyOGYxY2VhNmJhNjhkNGYzYTYyZTI3MzUxZTFhZCJ9.jMgz-2c2M8jmF-pepfaLohkEc75VeswUa7S1N6h6VgS7DOHw4rUVBjza_7VYEa_3gPpFHKiX_8OYI9_CjPJa4x1VU2encPP_FEHDU6m0lILEVVYkSV4LS7jfkJRe93GE51E8rQPUdR2IxcPc_P8mlE3Co-plLIktqAmW4_fyyMyLquDj-CjXfTpZ6RZs85rf3XH1qM-LOF8jhydDMKAhY8Iej4PCsT56C-kr26En3SqaA5BNgEftLIxhjQoFuZNCj8c7g5PuUztdmQ6UhjFA1-G392HnMx7rMYsfjeXxs2zaTeSjPh-z4JwyqkBTxZm6_rp37RpeQJFcGKzxWZNx8g

https://blog.damavis.com/los-principios-solid-ilustrados-en-ejemplos-sencillos-de-python/

https://ichi.pro/es/principios-solid-explicados-en-python-con-ejemplos-56291217871103


https://softwarecrafters.io/python/principios-solid-python
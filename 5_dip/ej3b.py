from abc import ABC, abstractmethod


class Switchable(ABC):
    @abstractmethod
    def turn_off(self):
        pass

    @abstractmethod
    def turn_on(self):
        pass

# low level


class LightBulb(Switchable):
    def turn_on(self):
        print("light bulb turned on")

    def turn_off(self):
        print("light bulb turn off")


# high level
class ElectricPowerSwitch:

    def __init__(self, switchable):
        self.switchable = switchable
        self.on = False

    def press(self):
        if self.on:
            self.switchable.turn_off()
            self.on = False
        else:
            self.switchable.turn_on()
            self.on = True

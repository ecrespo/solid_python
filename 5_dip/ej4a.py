class Garage:
    def __init__(self):
        self.cars = []

    def add_car_to_garage(self, name, capacity):
        self.cars.append((name, capacity))  # eg: ('Tesla1', 50)


class ChargingStation:
    def __init__(self, garage):
        num = len([car for car in garage.cars if car[1] < 50])
        print(f'We need to charge {num} car(s)')


gar = Garage()
gar.add_car_to_garage('Tesla1', 50)
gar.add_car_to_garage('Tesla2', 30)
gar.add_car_to_garage('Tesla1', 80)
station = ChargingStation(gar)  # print We need to charge 1 car(s)

# https://betterprogramming.pub/the-single-responsibility-principle-explained-in-python-622e2d996d86

class Car:
    prices = {'BMW': 100000, 'Audi': 200000, 'Mercedes': 300000}

    def __init__(self, name):

        if name not in self.prices:
            print("Sorry, we don't have this car")
            return

        self.name = name
        self.price = self.prices[name]

    def testDrive(self):
        print("Driving {}".format(self.name))

    def buy(self, cash):
        if cash < self.price:
            print("Sorry, you don't have enough money")
        else:
            print("Buying {}".format(self.name))


if __name__ == '__main__':
    car = Car('BMW')
    car.buy(100000)

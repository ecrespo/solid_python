# SRP (Single Responsible Principle)

El primer principio de SOLID llamado Principio de Responsabilidad Única indica que una clase debería ser responsable de una única funcionalidad. En otras palabras, la clase solo debería tener una única razón para cambiar.


* Una clase = Un concepto y responsabilidad

* Una clase debería tener sólo 1 razón para cambiar
* Cómo:
    * Clases pequeñas con objetivos acotados
* Si es que tienes más de un método público puede ser señal de más de una responsabilidad
* Nombres de servicios muy abstractos EmailService vs EmailSender (solo enviará emails)
* Finalidad: - Alta cohesión y robustez - Permitir composición de clases (inyección) - Reducir duplicidad
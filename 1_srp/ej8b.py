class ConfirmationMailMailer:
    def __init__(
        self,
        confirmation_mailer_factory: ConfirmationMailerFactory,
        mailer: MailerInterface
    ):
        self._mailer = mailer
        self._confirmation_mailer_factory = confirmation_mailer_factory

    def sent_to(self, user):
        message = self._create_message_for(user)
        self.send_message(message)

    def _create_message_for(self, user):
        return self._confirmation_mailer_factory.create_message_for(user)

    def send_message(self, message):
        self._mailer.send(message)


class ConfirmationMailFactory:
    def __init__(
        self,
        template_engine: TemplateEngineInterface,
        translator: TranslatorInterface
    ):
        self._template_engine = template_engine
        self._translator = translator

    def create_message_for(self, user):
        # Create an instance of Message based on the given User
        message = ...
        return message
    )

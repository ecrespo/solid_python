# https://medium.com/the-brainwave/how-to-write-clean-code-in-python-with-solid-principles-principle-1-e5b0d2e6469f#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6ImQ2M2RiZTczYWFkODhjODU0ZGUwZDhkNmMwMTRjMzZkYzI1YzQyOTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJuYmYiOjE2NDY5NjE1OTUsImF1ZCI6IjIxNjI5NjAzNTgzNC1rMWs2cWUwNjBzMnRwMmEyamFtNGxqZGNtczAwc3R0Zy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMzA0ODgwMTUxNTE4Mzk4NTA5MyIsImVtYWlsIjoiZWNyZXNwb0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMjE2Mjk2MDM1ODM0LWsxazZxZTA2MHMydHAyYTJqYW00bGpkY21zMDBzdHRnLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwibmFtZSI6IkVybmVzdG8gQ3Jlc3BvIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdpNzZJNGhTTXQ3U1hYbzZjdHdaNDloeDlKbTF3cU93UXZiTU0zbWtHMD1zOTYtYyIsImdpdmVuX25hbWUiOiJFcm5lc3RvIiwiZmFtaWx5X25hbWUiOiJDcmVzcG8iLCJpYXQiOjE2NDY5NjE4OTUsImV4cCI6MTY0Njk2NTQ5NSwianRpIjoiNTUxZmZlYTA3ZDhjZTMwNTdhY2QzODUxMDgyMmE0NmIyYTYyMmIyYiJ9.aF_8k16FjrNGyCbhAp0PxPYM6gtdLDLwD_Wlpdm3sschSsbuad-sOmIFOTs1HCp8VvI1DEGqHkV6ZilYTScFg9EDMZV1_4aVK--NZBacIBKQLyqPlnOwRjCI6HLiSZfUjMjO7KwSMwaL88sxywh3LaHUbQmY1G-zlVSlt0qTplJG9_IePwJ7-HoAkCsA44LEaWvdIg4M0Sls4NZTuFbpFgm5K44FuEkinEv5wPt-in2-sd_QNXaMN6UsOsLptRNEwo7YREswAFJJvPti57Y2vH4uyE0Sz27BwI1KbDsZKwdQLJ7tdsUrRA3QZNzwGGRDz_OMI5C-SmI4vfdALaAY7A


class ConfirmationMailMailer:
    def __init__(
        self,
        template_engine: TemplateEngineInterface,
        translator: TranslatorInterface,
        mailer: MailerInterface
    ):
        self._template_engine = template_engine
        self._translator = translator
        self._mailer = mailer

    def sent_to(self, user):
        message = self._create_message_for(user)
        self.send_message(message)

    def _create_message_for(self, user):
        subject = self._translator.translate("Confirm your email address")
        body = self._template_engine.render(
            "confirmation_email.html.tpl", user.get_confirmation_code())
        message = Message(subject, body)
        message.set_to(user.email)
        return message

    def send_message(self, message):
        self._mailer.send(message)

class Car:
    prices = {'BMW': 200000, 'Audi': 200000, 'Mercedes': 300000}

    def __init__(self, name):
        if name not in self.prices:
            print("Sorry, we don't have this car")
            return

        self.name = name
        self.price = self.prices[name]

    def testDrive(self):

        print("Driving {}".format(self.name))


class Finances:
    def buy(car, cash):

        if cash == car.price:
            print("Buying {}".format(car.name))
        elif cash > car.price/3:
            print("Buying {} on installments".format(car.name))
        else:
            print("Sorry, you don't have enough money")


if __name__ == '__main__':
    car = Car('BMW')
    Finances.buy(car, 100000)

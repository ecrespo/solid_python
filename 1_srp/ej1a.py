# Below is Given a class which has two responsibilities
class User:
    def __init__(self, name: str):
        self.name = name

    def get_name(self) -> str:
        pass

    def save(self, user: User):
        pass

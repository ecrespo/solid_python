class Vehicle(object):
    def __init__(self, name, engine):
        self._name = name
        self._engine = engine

    def getName(self):
        return self._name()

    def getEngineRPM(self):
        return self._engine.getRPM()

    def getMaxSpeed(self):
        return self._speed


class VehicleRepository(object):
    def __init__(self, vehicle, db)
    self._persistence = db
    self._vehicle = vehicle


class VehiclePrinter(object):
    def __init__(self, vehicle, db)
    self._persistence = db
    self._vehicle = vehicle

    def print(self):
    return print ‘Vehicle: {}, Max Speed: {}, RMP: {}’.format(self._vehicle.getName(), self._vehicle.getMaxSpeed(), self._vehicle.getRPM())
